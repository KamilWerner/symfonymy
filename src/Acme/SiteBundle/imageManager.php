<?php

namespace Acme\SiteBundle;

use Acme\SiteBundle\Entity\Image;

class imageManager {
	protected $imagesDir;
	
	function __construct($imagesDir)
	{
		$this->imagesDir = $imagesDir;	
	}
	
	public function checkExtension(Image $image)
	{
		$extension = strtolower($image->getFileExtension());
		if($extension == "jpg" || $extension == "png" || $extension == "gif" || $extension == "jpeg") return true;
		else return false;
	}
	
	public function resize(Image $image)
	{
		$img = $this->imagesDir."".$image->getPath();
		list($width_org, $height_org) = getimagesize($img);
		if($width_org>600 || $height_org>2000)
		{
			$extension = explode(".",strtolower($image->getPath()));
			$width = 600;
			$height = 2000;
			$ratio_org = $width_org/$height_org;
			if($width_org>600)$height = $width/$ratio_org;
			if($height_org>2000)$width = $height*$ratio_org;
	
			$image_p = imagecreatetruecolor($width, $height);
			if($extension[1] == "jpg" || $extension[1] == "jpeg")$imagenew = imagecreatefromjpeg($img);
			else if($extension[1] == "png")$imagenew = imagecreatefrompng($img);
			else if($extension[1] == "gif")$imagenew = imagecreatefromgif($img);
			else return null;
			imagecopyresampled($image_p, $imagenew, 0, 0, 0, 0, $width, $height, $width_org, $height_org);
			imagejpeg($image_p,$img,100);
		}
	}
	
	public function uploadFromLink(Image $image)
	{
		$filename = rand(0,1000000000);
		$extension = $image->getLink();
		$extension = explode(".", $extension);
		$extension = strtolower($extension[sizeof($extension)-1]);
		if($extension == "jpg" || $extension == "png" || $extension == "gif" || $extension == "jpeg"){
			copy($image->getLink(),$this->imagesDir."".$filename.".".$extension);
			$image->setPath($filename.".".$extension);
			return $image;
		}
		else return false;
	}
}