<?php

namespace Acme\SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
	/**
	* @Route("/user")
	*/
	public function userAction(Request $request)
	{
		$session = $request->getSession();
		$id = $session->get('id');
		if($id == null) return $this->redirect("/login");
		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery('SELECT i FROM AcmeSiteBundle:Image i WHERE i.userId = '.$id.' ORDER BY i.id DESC');
		$images = $query->getResult();
		if($images == null) return $this->render("user.html.twig",array('message' => "Nie masz żadnych obrazków"));
		return $this->render("user.html.twig",array('images'=>$images));
	}
}