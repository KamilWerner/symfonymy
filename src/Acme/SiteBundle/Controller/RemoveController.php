<?php

namespace Acme\SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RemoveController extends Controller
{
	/**
	* @Route("/remove/{id}")
	*/

	public function removeAction($id)
	{
		if($this->get('session')->isStarted())
		{
			$respository = $this->getDoctrine()->getRepository('AcmeSiteBundle:Image');
			$image = $respository->find($id);

			if(!$image) return $this->redirect('/user');

			if($image->getUserId() == $this->get('session')->get('id'))
			{
				$em = $this->getDoctrine()->getManager();
				$em->remove($image);
				$em->flush();
				$this->get('session')->getFlashBag()->add('notice','Usunięto zdjęcie');
			}
			return $this->redirect('/user');
		}
		return $this->redirect('/login');
	}
}