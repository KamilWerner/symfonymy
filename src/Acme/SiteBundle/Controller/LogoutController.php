<?php

namespace Acme\SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class LogoutController extends Controller
{
	/**
	* @Route("/logout")
	*/
	public function logoutAction(Request $request)
	{
		$session = $request->getSession();
		$session->clear();
		return $this->redirect("/login");
	}
}