<?php

namespace Acme\SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Acme\SiteBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class RegisterController extends Controller
{
	/**
	* @Route("/register")
	*/
	public function registerAction(Request $request)
	{
		$username = $request->request->get('username');
		$pass = $request->request->get('password');
		
		if(!$username || !$pass) return $this->render("error.html.twig", array('message'=>'Uzupełnij dane do rejestracji', 'title'=>'Wystąpił błąd'));
		$user = new User();
		$user->setUsername(strtolower($username));
		$user = $this->coder($pass, $user);

		$em = $this->getDoctrine()->getManager();
		$query = $em->createQuery('SELECT u.username FROM AcmeSiteBundle:User u');
		$allUsers = $query->getResult();

		for($i=0;$i<sizeof($allUsers);$i++)
		{
			if($allUsers[$i]['username'] == $user->getUsername()) return $this->render("error.html.twig", array('message'=>'Konto z takim loginem już istnieje', 'title'=>'Wystąpił błąd'));
		}

		$em->persist($user);
		$em->flush();

		return $this->render("error.html.twig", array('message'=>'Zarejestrowano pomyślnie', 'title'=>'Rejestracja'));
	}
	
	public function coder($password,$user)
	{
		$encoder = $this->container->get('security.password_encoder');
		$encoded = $encoder->encodePassword($user, $password);
		$user->setPassword($encoded);
		return $user;
	}
	
	public function validation($user) {
		
	}
}