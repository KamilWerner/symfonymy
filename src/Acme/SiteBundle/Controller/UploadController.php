<?php

namespace Acme\SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Acme\SiteBundle\Entity\Image;
use Acme\SiteBundle\imageManager;

class UploadController extends Controller
{
	/**
	* @Route("/upload/{type}")
	*/
	public function uploadAction($type)
	{
		if(!$this->get('session')->isStarted()) return $this->redirect("/login");
		$image = new Image();
		$im = new imageManager($this->getParameter("images_dir"));
		if($type=="disc")
		{
			$form = $this->createFormBuilder($image)
				->add('name')
				->add('desc')
				->add('file')
				->getForm();

			if($this->getRequest()->getMethod() === 'POST')
			{
				$form->bind($this->getRequest());
				if($form->isValid())
				{
					$checkExtension = $im->checkExtension($image);
					if(!$checkExtension){
						$this->addFlash("error", "Nieprawidłowe rozszerzenie pliku. Wybierz zdjęcie.");
						return $this->redirect("/user");
					}
					$image = $this->setImage($image);
					$image = $this->pushToDatabase($image);
					$image = $im->resize($image);
					return $this->redirect("/user");
				}
			}
		}
		else if($type="link")
		{
			$form = $this->createFormBuilder($image)
				->add('name')
				->add('desc')
				->add('link')
				->getForm();

			if($this->getRequest()->getMethod() == 'POST')
			{
				$form->bind($this->getRequest());
				if($form->isValid())
				{
					$image = $this->setImage($image);
					$image = $im->uploadFromLink($image);
					if(!$image){
						$this->addFlash("error", "Nieprawidłowe rozszerzenie pliku. Wybierz zdjęcie.");
						return $this->redirect("/user");
					}
					$image = $this->pushToDatabase($image);
					$image = $im->resize($image);
					return $this->redirect("/user");
				}
			}
		}
		return $this->render("upload.html.twig",array('form'=>$form->createView()));
	}

	public function setImage(Image $image)
	{
		$image->setType('image');
		$image->setUserId($this->get('session')->get('id'));
		$image->setDate(date("d-m-Y"));
		$image->setIp($_SERVER['REMOTE_ADDR']);
		if($image->getLink() == NULL) $image->setLink("-");
		return $image;
	}
	
	public function pushToDatabase(Image $image)
	{
		$em = $this->getDoctrine()->getEntityManager();
		$em->persist($image);
		$em->flush();
		return $image;
	}
}