<?php

namespace Acme\SiteBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Acme\SiteBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class CheckController extends Controller
{
	/**
	* @Route("/check")
	*/
	public function checkAction(Request $request)
	{
		$username = strtolower($request->request->get('username'));
		$pass = $request->request->get('password');
		if(!$username || !$pass) return $this->render("error.html.twig", array('message'=>'Wypełnij wszystkie pola', 'title'=>'Wystąpił błąd'));
		$user = new User();
		$user->setUsername($username);
		$user->setPassword($pass);

		$repo = $this->getDoctrine()->getRepository('AcmeSiteBundle:User');
		$findUser = $repo->findOneBy(array('username'=>$user->getUsername()));

		if($findUser != null)
		{
			$validation = $this->coder($user->getPassword(),$findUser);
			if($validation == true)
			{
				if($this->get('session')->isStarted() == false) $this->setSession($findUser->getId(),$findUser->getUsername(),$request);
				else $this->setSession($findUser->getId(),$findUser->getUsername(),$request);
				$this->get('session')->getFlashBag()->add('success', 'Pomyślnie zalogowano użytkownika');
				return $this->redirect('/user');
			}
		}
		return $this->render("error.html.twig", array('message'=>'Złe dane do logowania', 'title'=>'Wystąpił błąd'));
	}

	public function setSession($id,$login, Request $request)
	{
		$session = $request->getSession();
		$session->set('id', $id);
		$session->set('authorization', 'yes');
		$session->set('login', $login);
	}
	
	public function coder($password,$user)
	{
		$encoder = $this->get('security.password_encoder');
		if($encoder->isPasswordValid($user,$password))return true;
	}
}